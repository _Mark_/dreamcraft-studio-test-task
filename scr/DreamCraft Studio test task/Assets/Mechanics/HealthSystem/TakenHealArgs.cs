﻿using System;

namespace Mechanics.HealthSystem
{
	public sealed class TakenHealArgs : EventArgs
	{
		public int HealValueValue;

		public TakenHealArgs(int healValue)
		{
			HealValueValue = healValue;
		}
	}
}