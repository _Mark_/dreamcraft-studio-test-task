﻿using System;

namespace Mechanics.HealthSystem
{
	public interface IHealth
	{
		int Value { get; }
		int Min { get; }
		int Max { get; }
		bool IsDead { get; }
		void Reset();
		void Heal(int heal);
		void TakeDamage(int damage);

		event Action Death;
		event EventHandler<TakenHealArgs> TakenHeal;
		event EventHandler<TakenDamageArgs> TakenDamage;
		void Kill();
	}
}