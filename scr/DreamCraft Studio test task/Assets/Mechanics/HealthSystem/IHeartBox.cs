﻿namespace Mechanics.HealthSystem
{
	public interface IHeartBox
	{
		IHealth Health { get; }
	}
}