﻿using System;
using UnityEngine;

namespace Mechanics
{
	[Serializable]
	public class Cooldown
	{
		[SerializeField] private float _cooldown = .5f;

		public bool IsReady { get; private set; }
		public float Timer { get; private set; }

		public void Reset()
		{
			Timer = 0;
			IsReady = false;
		}

		public void Update(float deltaTime)
		{
			if (Timer > _cooldown) return;

			Timer += deltaTime;
			IsReady = Timer >= _cooldown;
		}
	}
}