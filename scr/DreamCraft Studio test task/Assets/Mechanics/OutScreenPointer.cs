﻿using System;
using Infrastructure;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Mechanics
{
	public class OutScreenPointer : MonoBehaviour, IInject
	{
		[SerializeField] private int _minIndent = 200;
		[SerializeField] private int _maxIndent = 200;
		[SerializeField] private LayerMask _contactLayers;
		[SerializeField] private Camera _camera;

		public void Inject(DependencyContainer dependencyContainer)
		{
			_camera = dependencyContainer.MainCamera;
		}
		
		public bool TryGetSpawnPosition(out Vector3 position)
		{
			var screenPosition = ScreenPosition();

			if (TryGetLookTargetHit(screenPosition, out var hit))
			{
				position = hit.point;
				return true;
			}

			position = Vector3.zero;
			return false;
		}

		private Vector2 ScreenPosition()
		{
			var side = RandomScreenSide();
			var screenPosition = RandomCoordinate(side);
			return screenPosition;
		}

		private Vector2 RandomCoordinate(EScreenSide side)
		{
			var x = 0f;
			var y = 0f;
			var screenWidth = _camera.pixelWidth;
			var screenHeight = _camera.pixelHeight;
			var indentSum = _minIndent + _maxIndent;

			switch (side)
			{
				case EScreenSide.Up:
					x = Random.Range(0 - indentSum, screenWidth + indentSum);
					y = Random.Range(screenHeight + _minIndent, screenHeight + indentSum);
					break;

				case EScreenSide.Down:
					x = Random.Range(0 - indentSum, screenWidth + indentSum);
					y = Random.Range(0 - _minIndent, 0 - indentSum);
					break;

				case EScreenSide.Left:
					x = Random.Range(0 - _minIndent, 0 - indentSum);
					y = Random.Range(0 - indentSum, screenHeight + indentSum);
					break;

				case EScreenSide.Right:
					x = Random.Range(screenWidth + _minIndent, screenWidth + indentSum);
					y = Random.Range(0 - indentSum, screenHeight + indentSum);
					break;

				default:
					Debug.LogError($"Incorrect screen side: \"{side.ToString()}\"!");
					break;
			}

			return new Vector2(x, y);
		}

		private static EScreenSide RandomScreenSide()
		{
			var sideNum = Enum.GetNames(typeof(EScreenSide)).Length;
			var side = (EScreenSide)Random.Range(0, sideNum);
			return side;
		}

		private bool TryGetLookTargetHit(Vector2 screenPosition, out RaycastHit hit)
		{
			var ray = RayFromCamera(screenPosition);
			return Physics.Raycast(ray, out hit, float.PositiveInfinity,
				_contactLayers, QueryTriggerInteraction.Ignore);
		}

		private Ray RayFromCamera(Vector2 screenPosition) =>
			_camera.ScreenPointToRay(screenPosition);
	}
}