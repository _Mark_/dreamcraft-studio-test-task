﻿using UnityEngine;

namespace Mechanics.EnemySystem
{
	public class EnemyKamikaze : Enemy
	{
		[SerializeField] private EnemyExplosion _enemyExplosion;
		
		protected override void OnInitialized(Transform target)
		{
			_enemyExplosion.Initialize(target, _health);
		}
	}
}