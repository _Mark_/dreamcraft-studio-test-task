﻿using UnityEngine;
using UnityEngine.AI;

namespace Mechanics.EnemySystem
{
	[RequireComponent(typeof(NavMeshAgent))]
	public class EnemyTargetFollow : MonoBehaviour
	{
		private Transform _target;
		private NavMeshAgent _agent;

		private void Awake()
		{
			_agent = GetComponent<NavMeshAgent>();
		}

		public void Initialize(Transform target)
		{
			_target = target;
			_agent.SetDestination(_target.position);
		}
		
		private void Update()
		{
			var position = transform.position;
			var targetPosition = _target.position;
			var distance = Vector3.Distance(position, targetPosition);

			if (distance <= _agent.stoppingDistance)
				return;

			_agent.SetDestination(targetPosition);
		}
	}
}