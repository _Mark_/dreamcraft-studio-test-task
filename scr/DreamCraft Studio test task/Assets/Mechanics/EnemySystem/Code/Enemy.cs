﻿using Mechanics.HealthSystem;
using UnityEngine;

namespace Mechanics.EnemySystem
{
	[RequireComponent(typeof(EnemyTargetFollow))]
	public class Enemy : MonoBehaviour
	{
		[SerializeField] protected Health _health;

		private EnemyTargetFollow _enemyTargetFollow;

		public event DeathEventHandler Death;

		private void Awake()
		{
			_enemyTargetFollow = GetComponent<EnemyTargetFollow>();

			_health.Death += OnDeath;
		}

		private void OnDestroy()
		{
			_health.Death -= OnDeath;
		}

		public void Initialize(Transform target)
		{
			RestoreHealth();
			_enemyTargetFollow.Initialize(target);
			OnInitialized(target);
		}

		private void RestoreHealth() =>
			_health.Reset();

		protected virtual void OnInitialized(Transform target) { }

		private void OnDeath() =>
			Death?.Invoke(this);
	}
}