﻿using System;
using Infrastructure;
using UnityEngine;

namespace Mechanics.EnemySystem
{
	[RequireComponent(typeof(OutScreenPointer))]
	public class EnemySpawner : MonoBehaviour, IInject
	{
		[SerializeField] private int _maxEnemyNum = 50;
		[SerializeField] private int _maxKamikazeNum = 10;
		[SerializeField] private Enemy _enemyPrefab;
		[SerializeField] private EnemyKamikaze _enemyKamikazePrefab;
		[SerializeField] private Transform _enemyTarget;
		[SerializeField] private Cooldown _spawnCooldown;

		private int _enemyCount;
		private LimitedPool<Enemy> _enemyPool;
		private LimitedPool<EnemyKamikaze> _enemyKamikazePool;
		private OutScreenPointer _spawnPointer;
		private IDependencyResolver _dependencyResolver;

		public void Inject(DependencyContainer dependencyContainer)
		{
			_enemyTarget = dependencyContainer.Player.Object.transform;
			_dependencyResolver = dependencyContainer.Resolver;
		}

		private void Awake()
		{
			_spawnPointer = GetComponent<OutScreenPointer>();
		}

		private void Start()
		{
			_enemyPool =
				new LimitedPool<Enemy>(_maxEnemyNum - _maxKamikazeNum, CreateEnemy);
			_enemyKamikazePool =
				new LimitedPool<EnemyKamikaze>(_maxKamikazeNum, CreateEnemyKamikaze);
		}

		private void Update()
		{
			_spawnCooldown.Update(Time.deltaTime);

			if (_spawnCooldown.IsReady && _enemyCount <= _maxEnemyNum)
			{
				_spawnCooldown.Reset();

				if (_enemyKamikazePool.IsCanPop)
				{
					Spawn(_enemyKamikazePool);
					return;
				}

				Spawn(_enemyPool);
			}
		}

		private void Spawn<TEnemy>(LimitedPool<TEnemy> pool) where TEnemy : Enemy
		{
			if (_spawnPointer.TryGetSpawnPosition(out var spawnPosition)
			    && pool.TryPop(out var enemy))
			{
				enemy.gameObject.SetActive(true);
				enemy.Initialize(_enemyTarget);
				enemy.transform.position = spawnPosition;
				_enemyCount++;
			}
		}

		private Enemy CreateEnemy()
		{
			var enemy = Create(_enemyPrefab);
			return enemy;
		}

		private EnemyKamikaze CreateEnemyKamikaze()
		{
			var enemy = Create(_enemyKamikazePrefab);
			return enemy;
		}

		private TEnemy Create<TEnemy>(TEnemy prefab) where TEnemy : Enemy
		{
			var enemy = Instantiate(prefab, Vector3.zero, Quaternion.identity);
			_dependencyResolver.ResolveFor(enemy.gameObject);
			enemy.Initialize(_enemyTarget);
			enemy.Death += OnDeath;
			enemy.gameObject.SetActive(false);
			return enemy;
		}

		private void OnDeath(Enemy sender)
		{
			sender.gameObject.SetActive(false);
			_enemyPool.Push(sender);
			_enemyCount--;
		}
	}
}