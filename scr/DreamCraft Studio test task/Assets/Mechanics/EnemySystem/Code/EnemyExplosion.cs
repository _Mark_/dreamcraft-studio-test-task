﻿using Mechanics.HealthSystem;
using Mechanics.ShootSystem;
using UnityEngine;

namespace Mechanics.EnemySystem
{
	public class EnemyExplosion : MonoBehaviour
	{
		[SerializeField] private float _explosionDistance = 10;
		[SerializeField] private Transform _target;
		[SerializeField] private OneBulletGun[] _guns;
		
		private IHealth _health;

		public void Initialize(Transform target, IHealth health)
		{
			_health = health;
			_target = target;
		}

		private void Update()
		{
			var position = transform.position;
			var targetPosition = _target.position;
			var distance = Vector3.Distance(position, targetPosition);

			if (distance <= _explosionDistance)
				Explosion();
		}

		private void Explosion()
		{
			foreach (var gun in _guns) 
				gun.Shoot();
			
			_health.Kill();
		}
	}
}