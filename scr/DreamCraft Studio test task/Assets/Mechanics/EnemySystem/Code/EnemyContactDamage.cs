﻿using UnityEngine;

namespace Mechanics.EnemySystem
{
	public class EnemyContactDamage : ContactDamage
	{
		[SerializeField] private Cooldown _cooldown;

		private void Update() =>
			_cooldown.Update(Time.deltaTime);

		protected override bool IsCanApplyDamage(Collision collision)
		{
			if (_cooldown.IsReady == false
			    || collision.gameObject.TryGetComponent<Enemy>(out var enemy))
			{
				return false;
			}

			return true;
		}

		protected override void OnApplyingDamage()
		{
			_cooldown.Reset();
		}
	}
}