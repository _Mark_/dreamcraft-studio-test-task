﻿using Infrastructure;
using Mechanics.HealthSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Mechanics.EnemySystem
{
	[RequireComponent(typeof(Slider))]
	public class HealthBar : MonoBehaviour, IInject
	{
		private IHealth _health;
		
		private Slider _slider;

		public void Inject(DependencyContainer dependencyContainer)
		{
			_health = dependencyContainer.Player.Health;
			
			_health.TakenHeal += OnTakenHeal;
			_health.TakenDamage += OnTakenDamage;
		}
		
		private void Awake()
		{
			_slider = GetComponent<Slider>();
		}

		private void OnDestroy()
		{
			_health.TakenHeal -= OnTakenHeal;
			_health.TakenDamage -= OnTakenDamage;
		}

		private void OnTakenDamage(object sender, TakenDamageArgs args) =>
			SetValue();

		private void OnTakenHeal(object sender, TakenHealArgs args) =>
			SetValue();

		private void SetValue()
		{
			var healthRation = (float) _health.Value / _health.Max;
			_slider.value = healthRation;
		}
	}
}