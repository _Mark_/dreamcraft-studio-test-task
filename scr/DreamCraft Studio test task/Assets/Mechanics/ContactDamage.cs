﻿using System;
using Mechanics.HealthSystem;
using UnityEngine;

namespace Mechanics
{
	public class ContactDamage : MonoBehaviour
	{
		[SerializeField] private int _damage = 5;
		[SerializeField] private CollisionHandler _collisionHandler;

		public event Action DamageDone;

		public int Damage
		{
			get => _damage;
			set => _damage = value;
		}

		private void Awake()
		{
			_collisionHandler.CollisionStay += OnCollisionDetect;
		}

		private void OnCollisionDetect(Collision collision)
		{
			if (collision.gameObject.TryGetComponent<IHealth>(out var health))
			{
				var isCanAttack = IsCanApplyDamage(collision);
				if (isCanAttack == false)
					return;

				OnApplyingDamage();
				health.TakeDamage(_damage);
				DamageDone?.Invoke();
			}
		}

		protected virtual bool IsCanApplyDamage(Collision collision) => true;
		protected virtual void OnApplyingDamage() { }
	}
}