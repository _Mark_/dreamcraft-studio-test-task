﻿using System.Collections;
using Infrastructure;
using Mechanics.Service;
using UnityEngine;

namespace Mechanics.ShootSystem
{
	public abstract class BaseWeapon : MonoBehaviour, IWeapon, IInject
	{
		[SerializeField] protected int _damage = 50;
		[SerializeField] protected float _bulletLifeTime = 3;
		[SerializeField] protected Transform _rootTransform;
		[SerializeField] protected Cooldown _cooldown;
		[SerializeField] protected BulletLauncher _bulletLauncher;

		private BulletPool _bulletPool;
		private ICoroutineRunner _coroutineRunner;

		public void Inject(DependencyContainer dependencyContainer)
		{
			_bulletPool = dependencyContainer.BulletPool;
			_coroutineRunner = dependencyContainer.CoroutineRunner;
		}

		private void Start()
		{
			_bulletLauncher.Construct(_bulletPool);
		}

		public abstract void Shoot();

		private void Update()
		{
			_cooldown.Update(Time.deltaTime);
		}

		protected void TrackBy(Bullet bullet) =>
			_coroutineRunner.StartCoroutine(ReturnBulletToPool(bullet));

		private IEnumerator ReturnBulletToPool(Bullet bullet)
		{
			var timer = 0f;
			do
			{
				yield return null;
				timer += Time.deltaTime;
			} while (timer < _bulletLifeTime);

			bullet.gameObject.SetActive(false);
			_bulletPool.Push(bullet);
		}
	}
}