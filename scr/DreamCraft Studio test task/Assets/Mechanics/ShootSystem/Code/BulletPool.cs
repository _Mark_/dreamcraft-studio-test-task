﻿using Infrastructure;
using UnityEngine;

namespace Mechanics.ShootSystem
{
	public class BulletPool : MonoBehaviour
	{
		[SerializeField] private int _poolExpander = 10;
		[SerializeField] private int _bulletPoolCapacity = 100;
		[SerializeField] private Bullet _bulletPrefab;

		private LimitedPool<Bullet> _pool;

		private void Awake()
		{
			_pool = new LimitedPool<Bullet>(_bulletPoolCapacity, CreateBullet);
		}

		public Bullet Pop()
		{
			if (_pool.IsCanPop == false)
			{
				_pool.Expand(_poolExpander);
			}

			_pool.TryPop(out var bullet);
			return bullet;
		}

		public void Push(Bullet bullet)
		{
			_pool.Push(bullet);
		}
		
		private Bullet CreateBullet()
		{
			var bullet = Instantiate(_bulletPrefab);
			bullet.gameObject.SetActive(false);
			return bullet;
		}
	}
}