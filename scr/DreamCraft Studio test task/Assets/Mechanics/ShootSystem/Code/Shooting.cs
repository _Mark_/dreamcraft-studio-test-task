﻿using Infrastructure;
using Mechanics.Service.Input;
using UnityEngine;

namespace Mechanics.ShootSystem
{
	public class Shooting : MonoBehaviour, IInject
	{
		[SerializeField] private bool _isEnabled = true;

		private IInputService _inputService;

		public IWeapon CurrentWeapon { get; set; }

		public bool IsEnabled
		{
			get => _isEnabled;
			set => _isEnabled = value;
		}

		public void Inject(DependencyContainer dependencyContainer)
		{
			_inputService = dependencyContainer.InputService;
			_inputService.FirePressed += OnFirePressed;
		}
		
		private void OnFirePressed()
		{
			if (_isEnabled == false) return;

			Shoot();
		}

		private void Shoot()
		{
			CurrentWeapon.Shoot();
		}
	}
}