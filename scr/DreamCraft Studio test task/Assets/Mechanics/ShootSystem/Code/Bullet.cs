﻿using UnityEngine;

namespace Mechanics.ShootSystem
{
	[RequireComponent(typeof(ContactDamage))]
	public class Bullet : MonoBehaviour
	{
		private Rigidbody _rigidbody;
		private ContactDamage _contactDamage;

		private void Awake()
		{
			_rigidbody = GetComponent<Rigidbody>();
			_contactDamage = GetComponent<ContactDamage>();

			_contactDamage.DamageDone += OnDamageDone;
		}

		private void OnDestroy()
		{
			_contactDamage.DamageDone -= OnDamageDone;
		}

		public void SetDamage(int damage) =>
			_contactDamage.Damage = damage;

		public void SetVelocity(Vector3 velocity) =>
			_rigidbody.velocity = velocity;

		private void OnDamageDone() =>
			gameObject.SetActive(false);
	}
}