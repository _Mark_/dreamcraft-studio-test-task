﻿namespace Mechanics.ShootSystem
{
	public class OneBulletGun : BaseWeapon
	{
		public override void Shoot()
		{
			if (_cooldown.IsReady == false)
				return;

			_cooldown.Reset();
			var bullet =
				_bulletLauncher.LaunchBullet(_rootTransform.forward, _damage);
			TrackBy(bullet);
		}
	}
}