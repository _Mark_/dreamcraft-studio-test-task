﻿using System;
using UnityEngine;

namespace Mechanics.ShootSystem
{
	[Serializable]
	public class BulletLauncher
	{
		[SerializeField] private float _bulletSpeed = 1;
		[SerializeField] private Transform _launchPoint;

		private BulletPool _bulletPool;

		public Transform LaunchPoint => _launchPoint;

		public void Construct(BulletPool bulletPool) =>
			_bulletPool = bulletPool;

		public Bullet LaunchBullet(Vector3 direction, int damage)
		{
			var bullet = _bulletPool.Pop();
			InitializeBullet(bullet, direction, damage);
			return bullet;
		}

		private void InitializeBullet(Bullet bullet, Vector3 direction, int damage)
		{
			bullet.SetDamage(damage);
			SetPosition(bullet);
			SetVelocity(direction, bullet);
			ActivateBulletObject(bullet);
		}

		private void SetPosition(Bullet bullet) =>
			bullet.transform.position = LaunchPoint.position;

		private void SetVelocity(Vector3 direction, Bullet bullet)
		{
			var velocity = direction.normalized * _bulletSpeed;
			bullet.SetVelocity(velocity);
		}

		private static void ActivateBulletObject(Bullet bullet) =>
			bullet.gameObject.SetActive(true);
	}
}