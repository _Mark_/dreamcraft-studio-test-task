﻿using Extension;
using UnityEngine;

namespace Mechanics.ShootSystem
{
	public class FanBulletGun : BaseWeapon
	{
		[SerializeField] private int _bulletQuantity = 10;
		[SerializeField] private float _angleExpansion = 90;

		public override void Shoot()
		{
			if (_cooldown.IsReady == false)
				return;

			_cooldown.Reset();

			var up = _rootTransform.up;
			var forward = _rootTransform.forward;
			var section = _angleExpansion / _bulletQuantity - 1;
			var startAngel = _angleExpansion / 2;

			var direction = forward.ClockwiseRotate(startAngel, up);

			for (int i = 0; i < _bulletQuantity; i++)
			{
				var bullet = _bulletLauncher.LaunchBullet(direction, _damage);
				TrackBy(bullet);
				direction = direction.CounterclockwiseRotate(section, up);
			}
		}

		private void OnDrawGizmosSelected()
		{
			if (_bulletLauncher.LaunchPoint == null) return;

			var scale = 10;
			Gizmos.color = Color.red;
			var up = _rootTransform.up;
			var angle = _angleExpansion / 2;
			var origin = _bulletLauncher.LaunchPoint.position;
			var forward = _rootTransform.forward;

			var left = (forward * scale).ClockwiseRotate(angle, up);
			var right = (forward * scale).CounterclockwiseRotate(angle, up);

			Gizmos.DrawLine(origin, origin + left);
			Gizmos.DrawLine(origin, origin + right);
		}
	}
}