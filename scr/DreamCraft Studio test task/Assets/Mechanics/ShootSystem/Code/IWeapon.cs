﻿namespace Mechanics.ShootSystem
{
	public interface IWeapon
	{
		void Shoot();
	}
}