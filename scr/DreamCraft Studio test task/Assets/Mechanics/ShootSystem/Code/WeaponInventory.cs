﻿using System.Collections.Generic;
using Infrastructure;
using Mechanics.Service.Input;
using UnityEngine;

namespace Mechanics.ShootSystem
{
	public class WeaponInventory : MonoBehaviour, IInject
	{
		[SerializeField] private Shooting _shooting;

		private IInputService _inputService;
		private int _currentWeaponIndex = 0;
		private readonly List<IWeapon> _weapons = new List<IWeapon>();

		public void Inject(DependencyContainer dependencyContainer)
		{
			_inputService = dependencyContainer.InputService;
			_inputService.NextWeaponPressed += OnNextWeaponPressed;
		}

		private void Awake()
		{
			CollectWeapon();
		}

		private void Start()
		{
			SelectWeapon(0);
		}

		private void OnNextWeaponPressed()
		{
			SelectNextWeapon();
		}

		private void SelectNextWeapon()
		{
			_currentWeaponIndex = NextWeaponIndex();
			SelectWeapon(_currentWeaponIndex);
		}

		private void SelectWeapon(int weaponIndex)
		{
			if (_weapons.Count == 0)
				return;

			_shooting.CurrentWeapon = _weapons[weaponIndex];
		}

		private int NextWeaponIndex() =>
			_currentWeaponIndex + 1 < _weapons.Count ? _currentWeaponIndex + 1 : 0;

		private void CollectWeapon()
		{
			var weapons = GetComponentsInChildren<IWeapon>();
			_weapons.AddRange(weapons);
		}
	}
}