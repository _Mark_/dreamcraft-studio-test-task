﻿using System.Collections;
using UnityEngine;

namespace Mechanics.Service
{
	public interface ICoroutineRunner : IService
	{
		Coroutine StartCoroutine(IEnumerator routine);
	}
}