﻿using Infrastructure;
using Mechanics.CharacterSystem;
using Mechanics.HealthSystem;
using Mechanics.ShootSystem;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Mechanics.Service
{
	public class EndGame : MonoBehaviour, IService, IInject
	{
		private Button _reloadGameButton;
		private Canvas _reloadGameScreen;
		private IHealth _gameCharacterHealth;
		private Shooting _gameCharacterShooting;
		private ViewCharacter _viewCharacter;
		private CharacterMovement _characterMovement;

		public void Inject(DependencyContainer dependencyContainer)
		{
			_reloadGameScreen = dependencyContainer.ReloadGameScreen;
			_reloadGameButton = dependencyContainer.ReloadGameButton;
			
			_viewCharacter = dependencyContainer.Player.ViewCharacter;
			_characterMovement = dependencyContainer.Player.CharacterMovement;
			_gameCharacterHealth = dependencyContainer.Player.Health;
			_gameCharacterShooting = dependencyContainer.Player.Shooting;
			
			_gameCharacterHealth.Death += OnGameCharacterDeath;
			_reloadGameButton.onClick.AddListener(ReloadGameButtonClick);
		}

		private void OnDestroy()
		{
			_gameCharacterHealth.Death -= OnGameCharacterDeath;
			_reloadGameButton.onClick.RemoveListener(ReloadGameButtonClick);
		}

		private void OnGameCharacterDeath()
		{
			_viewCharacter.enabled = false;
			_characterMovement.enabled = false;
			_gameCharacterShooting.IsEnabled = false;

			_reloadGameScreen.gameObject.SetActive(true);
		}

		private void ReloadGameButtonClick()
		{
			ReloadGame();
		}

		private void ReloadGame()
		{
			SceneManager.LoadScene(0);
		}
	}
}