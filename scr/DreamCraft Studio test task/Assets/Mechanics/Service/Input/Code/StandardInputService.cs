﻿using System;
using UnityEngine;

namespace Mechanics.Service.Input
{
	public class StandardInputService : MonoBehaviour, IInputService
	{
		private StandardInputAction _inputAction;

		public event Action FirePressed;
		public event Action NextWeaponPressed;

		public bool IsMovementButtonsPressed { get; set; }
		public Vector2 MoveDirection { get; private set; }
		public Vector2 MousePosition { get; private set; }

		private void Awake()
		{
			_inputAction = new StandardInputAction();
			Initialize();
		}

		private void OnEnable() =>
			_inputAction.Enable();

		private void OnDisable() =>
			_inputAction.Disable();

		private void Initialize()
		{
			_inputAction.Player.Movement.performed +=
				context => MoveDirection = context.ReadValue<Vector2>();
			
			_inputAction.Player.WeaponSelector.performed +=
				context => NextWeaponPressed?.Invoke();
			
			_inputAction.Player.Fire.performed += context => FirePressed?.Invoke();
			
			_inputAction.Player.View.performed += context =>
				MousePosition = context.ReadValue<Vector2>();
		}

		private void Update()
		{
			IsMovementButtonsPressed = _inputAction.Player.Movement.IsPressed();
		}
	}
}