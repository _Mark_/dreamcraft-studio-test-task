﻿using System;
using UnityEngine;

namespace Mechanics.Service.Input
{
	public interface IInputService : IService
	{
		event Action FirePressed;
		bool IsMovementButtonsPressed { get; set; }
		Vector2 MoveDirection { get; }
		Vector2 MousePosition { get; }
		event Action NextWeaponPressed;
	}
}