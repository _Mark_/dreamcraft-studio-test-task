using Infrastructure;
using UnityEngine;

namespace Mechanics.SmartCamera
{
	public class CameraFollow : MonoBehaviour, IInject
	{
		[SerializeField] private float _damper = 2;
		[SerializeField] private Vector3 _offset;
		[SerializeField] private Transform _target;

		public void Inject(DependencyContainer dependencyContainer)
		{
			_target = dependencyContainer.Player.Object.transform;
		}

		private void LateUpdate()
		{
			var position = transform.position;
			var targetPosition = _target.position + _offset;

			transform.position = Vector3.Lerp(position, targetPosition,
				_damper * Time.deltaTime);

			transform.LookAt(_target.position);
		}
	}
}