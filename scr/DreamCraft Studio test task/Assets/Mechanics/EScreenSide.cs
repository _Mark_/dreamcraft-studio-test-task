﻿namespace Mechanics
{
	public enum EScreenSide
	{
		Up,
		Down,
		Left,
		Right,
	}
}