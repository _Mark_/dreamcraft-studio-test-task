﻿using Infrastructure;
using Mechanics.Service.Input;
using UnityEngine;
using UnityEngine.AI;

namespace Mechanics.CharacterSystem
{
	[RequireComponent(typeof(NavMeshAgent))]
	public class CharacterMovement : MonoBehaviour, IInject
	{
		[SerializeField] private Camera _camera;
		
		private NavMeshAgent _agent;
		private IInputService _inputService;

		public void Inject(DependencyContainer dependencyContainer)
		{
			_camera = dependencyContainer.MainCamera;
			_inputService = dependencyContainer.InputService;
		}

		private void Awake()
		{
			_agent = GetComponent<NavMeshAgent>();
			_agent.angularSpeed = 0;
		}

		private void Update()
		{
			if (_inputService.IsMovementButtonsPressed == false)
				return;

			var directionMovement = DirectionMovement();
			_agent.SetDestination(transform.position + directionMovement);
		}

		private Vector3 DirectionMovement()
		{
			var direction = _inputService.MoveDirection;
			var cameraDirection = _camera.transform.TransformVector(direction);
			cameraDirection = Vector3.ProjectOnPlane(cameraDirection, Vector3.up);
			cameraDirection.Normalize();
			return cameraDirection;
		}
	}
}