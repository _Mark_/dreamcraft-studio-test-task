﻿using Infrastructure;
using Mechanics.Service.Input;
using UnityEngine;

namespace Mechanics.CharacterSystem
{
	public class ViewCharacter : MonoBehaviour, IInject
	{
		[SerializeField] private LayerMask _contactLayers;
		[SerializeField] private Camera _camera;

		private IInputService _inputService;

		public void Inject(DependencyContainer dependencyContainer)
		{
			_camera = dependencyContainer.MainCamera;
			_inputService = dependencyContainer.InputService;
		}

		private void Update()
		{
			SetLookDirection();
		}

		private void SetLookDirection()
		{
			var mousePosition = _inputService.MousePosition;
			if (TryGetLookTargetHit(mousePosition, out var hit))
			{
				var direction = Direction(hit);
				transform.rotation = Quaternion.LookRotation(direction);
			}
		}

		private Vector3 Direction(RaycastHit hit)
		{
			var direction = hit.point - transform.position;
			direction.Scale(new Vector3(1, 0, 1));
			return direction;
		}

		private bool TryGetLookTargetHit(Vector2 screenPosition, out RaycastHit hit)
		{
			var ray = RayFromCamera(screenPosition);
			return Physics.Raycast(ray, out hit, float.PositiveInfinity,
				_contactLayers, QueryTriggerInteraction.Ignore);
		}

		private Ray RayFromCamera(Vector2 screenPosition) =>
			_camera.ScreenPointToRay(screenPosition);
	}
}