﻿using System.Collections.Generic;
using Extension;
using Mechanics.CharacterSystem;
using Mechanics.Constant;
using Mechanics.EnemySystem;
using Mechanics.HealthSystem;
using Mechanics.Service;
using Mechanics.Service.Input;
using Mechanics.ShootSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Infrastructure
{
	public class SceneLoader : MonoBehaviour, IDependencyResolver
	{
		[SerializeField] private Camera _mainCamera;
		[SerializeField] private Canvas _gameHud;
		[SerializeField] private Canvas _reloadGameScreen;
		[SerializeField] private GameObject _gameCharacter;
		[Header("Services")]
		[SerializeField] private EndGame _endGame;
		[SerializeField] private BulletPool _bulletPool;
		[SerializeField] private EnemySpawner _enemySpawner;
		[SerializeField] private StandardInputService _inputService;
		[SerializeField] private CoroutineRunnerService _coroutineRunner;

		private DependencyContainer _dependencyContainer;

		private void Awake()
		{
			if (TryFindInitialPoint(out var initialPoint) == false)
				return;

			_dependencyContainer = new DependencyContainer();

			CreateSceneObjects(initialPoint);
			CreateServices();

			ResolveDependencies();
		}

		public void ResolveFor(GameObject obj)
		{
			var injects = obj.GetComponentsInChildren<IInject>();
			ResolveFor(injects);
		}

		public void ResolveFor(IEnumerable<GameObject> objs)
		{
			foreach (var obj in objs)
			{
				var injects = obj.GetComponentsInChildren<IInject>();
				ResolveFor(injects);
			}
		}

		public void ResolveFor(IInject inject) =>
			inject.Inject(_dependencyContainer);

		public void ResolveFor(IEnumerable<IInject> injects)
		{
			foreach (var inject in injects)
				ResolveFor(inject);
		}

		private void CreateSceneObjects(GameObject initialPoint)
		{
			CreatePlayer(initialPoint);

			Instantiate(_gameHud);
			Instantiate(_endGame, transform);
			CreateReloadGameScreen();
			_dependencyContainer.MainCamera = Instantiate(_mainCamera);
		}

		private void CreateServices()
		{
			_dependencyContainer.BulletPool = Instantiate(_bulletPool);
			_dependencyContainer.InputService = Instantiate(_inputService, transform);
			_dependencyContainer.CoroutineRunner =
				Instantiate(_coroutineRunner, transform);

			Instantiate(_enemySpawner.transform);

			_dependencyContainer.Resolver = this;
		}

		private void ResolveDependencies()
		{
			var injects = this.FindObjectsOfType<IInject>();
			foreach (var inject in injects)
				inject.Inject(_dependencyContainer);
		}

		private void CreateReloadGameScreen()
		{
			var reloadGameScreen = Instantiate(_reloadGameScreen);
			reloadGameScreen.gameObject.SetActive(false);
			_dependencyContainer.ReloadGameScreen = reloadGameScreen;
			_dependencyContainer.ReloadGameButton =
				reloadGameScreen.GetComponentInChildren<Button>();
		}

		private void CreatePlayer(GameObject initialPoint)
		{
			var gameCharacterObject = Instantiate(_gameCharacter,
				initialPoint.transform.position, Quaternion.identity);

			_dependencyContainer.Player.Object = gameCharacterObject;

			_dependencyContainer.Player.CharacterMovement =
				gameCharacterObject.GetComponent<CharacterMovement>();

			_dependencyContainer.Player.ViewCharacter =
				gameCharacterObject.GetComponent<ViewCharacter>();

			_dependencyContainer.Player.Shooting =
				gameCharacterObject.GetComponentInChildren<Shooting>();

			_dependencyContainer.Player.Health =
				gameCharacterObject.GetComponent<IHealth>();
		}

		private static bool TryFindInitialPoint(out GameObject initialPoint)
		{
			initialPoint = GameObject.FindWithTag(Tag.INITIAL_POINT);
			if (initialPoint == null)
			{
				Debug.LogError(
					$"Can not to find game object with \"{Tag.INITIAL_POINT}\" tag!");
				return false;
			}

			return true;
		}
	}
}