﻿using System;
using System.Collections.Generic;

namespace Infrastructure
{
	public class LimitedPool<T>
	{
		private readonly Stack<T> _stack;
		private readonly Func<T> _creator;

		public LimitedPool(int startCapacity, Func<T> creator)
		{
			_stack = new Stack<T>(startCapacity);
			_creator = creator;

			FillStack(startCapacity);
		}

		public bool IsCanPop => _stack.Count > 0;

		public bool TryPop(out T element)
		{
			if (_stack.Count == 0)
			{
				element = default;
				return false;
			}

			element = _stack.Pop();
			return true;
		}

		public void Push(T element) =>
			_stack.Push(element);

		public void Expand(int value)
		{
			for (int i = 0; i < value; i++)
				Create();
		}

		private void FillStack(int startCapacity)
		{
			for (int i = 0; i < startCapacity; i++)
				Create();
		}

		private void Create()
		{
			var newElement = _creator.Invoke();
			_stack.Push(newElement);
		}

		private T GetNew() =>
			_creator.Invoke();
	}
}