﻿using Mechanics.Service;
using Mechanics.Service.Input;
using Mechanics.ShootSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Infrastructure
{
	public class DependencyContainer
	{
		public Player Player = new Player();
		public Camera MainCamera;
		public Canvas ReloadGameScreen;
		public Button ReloadGameButton;
		
		// Services
		public BulletPool BulletPool;
		public IInputService InputService;
		public ICoroutineRunner CoroutineRunner;
		public IDependencyResolver Resolver;
	}
}