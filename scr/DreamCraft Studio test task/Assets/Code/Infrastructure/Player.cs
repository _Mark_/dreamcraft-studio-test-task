﻿using Mechanics.CharacterSystem;
using Mechanics.HealthSystem;
using Mechanics.ShootSystem;
using UnityEngine;

namespace Infrastructure
{
	public class Player
	{
		public IHealth Health;
		public Shooting Shooting;
		public GameObject Object;
		public ViewCharacter ViewCharacter;
		public CharacterMovement CharacterMovement;
	}
}