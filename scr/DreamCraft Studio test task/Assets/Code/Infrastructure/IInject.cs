﻿namespace Infrastructure
{
	public interface IInject
	{
		void Inject(DependencyContainer dependencyContainer);
	}
}