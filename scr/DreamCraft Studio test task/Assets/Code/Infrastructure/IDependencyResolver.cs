﻿using System.Collections.Generic;
using UnityEngine;

namespace Infrastructure
{
	public interface IDependencyResolver
	{
		void ResolveFor(IInject inject);
		void ResolveFor(IEnumerable<IInject> injects);
		void ResolveFor(GameObject obj);
	}
}