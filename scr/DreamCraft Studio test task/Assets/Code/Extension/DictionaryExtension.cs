﻿using System.Collections.Generic;

namespace Extension
{
	public static class DictionaryExtension
	{
		public static Dictionary<TKey, TValue> AddNew<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value)
		{
			dictionary.Add(key, value);
			return dictionary;
		}
	}
}